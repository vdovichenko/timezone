package com.tag.timezoneex.activity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tag.timezoneex.R;
import com.tag.timezoneex.Utils;
import com.tag.timezoneex.ads.AdsHelperFacade;

public class MainActivity extends AppCompatActivity {
	private Spinner spinnerAvailableID;
	private Calendar current;
	private TextView textTimeZone, txtCurrentTime, txtTimeZoneTime;
    private Button bAbout, bCopy;
	private long milliSeconds;
	private ArrayAdapter<String> idAdapter;
	private SimpleDateFormat sdf;
	private Date resultDate;
	private boolean userSelect;

	/** Called when the activity is first created. */

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);

		spinnerAvailableID = (Spinner) findViewById(R.id.availableID);

		textTimeZone = (TextView) findViewById(R.id.timezone);
		txtCurrentTime = (TextView) findViewById(R.id.txtCurrentTime);
		txtTimeZoneTime = (TextView) findViewById(R.id.txtTimeZoneTime);

		String[] idArray = TimeZone.getAvailableIDs();

		sdf = new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm:ss", Locale.getDefault());

		idAdapter = new ArrayAdapter<>(this, R.layout.tvtimezone, idArray);

		idAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerAvailableID.setAdapter(idAdapter);
		spinnerAvailableID.setPrompt("Select time zone");

		getGMTTime();

		spinnerAvailableID
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
						if (userSelect) {
							//show interstitial ad
							AdsHelperFacade.showInterstitial(MainActivity.this);
							userSelect = false;
						}

						getGMTTime();
						String selectedId = (String) (parent
								.getItemAtPosition(position));

						TimeZone timezone = TimeZone.getTimeZone(selectedId);
						String TimeZoneName = timezone.getDisplayName();

//						int TimeZoneOffset = timezone.getOffset(0)//getRawOffset()
//								/ (60 * 1000);

//						int hrs = TimeZoneOffset / 60;
//						int mins = TimeZoneOffset % 60;

						long hrs = TimeUnit.MILLISECONDS.toHours(timezone.getOffset(System.currentTimeMillis()));
						long mins = TimeUnit.MILLISECONDS.toMinutes(timezone.getOffset(System.currentTimeMillis()))
								- TimeUnit.HOURS.toMinutes(hrs);
						// avoid -4:-30 issue
						mins = Math.abs(mins);

						milliSeconds = milliSeconds + timezone.getOffset(System.currentTimeMillis());//getRawOffset();

						resultDate = new Date(milliSeconds);
						System.out.println(sdf.format(resultDate));

						textTimeZone.setText(TimeZoneName + " : GMT " + hrs + "." + mins);
						txtTimeZoneTime.setText("" + sdf.format(resultDate));
						milliSeconds = 0;
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {}
				});

		spinnerAvailableID.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				userSelect = true;
				return false;
			}
		});

        bAbout = (Button) findViewById(R.id.bAbout);
        bAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
				Intent intent = new Intent(MainActivity.this, AboutActivity.class);
				startActivity(intent);
            }
        });

        bCopy = (Button) findViewById(R.id.bCopy);
        bCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
				//show rewarded ad
				AdsHelperFacade.showRewarded(MainActivity.this);

				if(Utils.copyToClipboard(MainActivity.this, txtTimeZoneTime.getText().toString())) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(MainActivity.this, MainActivity.this.getString(R.string.text_copied), Toast.LENGTH_SHORT).show();
						}
					});
				}
            }
        });
	}
	
	// Convert Local Time into GMT time
	private void getGMTTime() {
		current = Calendar.getInstance();
		txtCurrentTime.setText("" + current.getTime());

		milliSeconds = current.getTimeInMillis();

		TimeZone tzCurrent = current.getTimeZone();
		int offset = tzCurrent.getRawOffset();
		if (tzCurrent.inDaylightTime(new Date())) {
			offset = offset + tzCurrent.getDSTSavings();
		}

		milliSeconds = milliSeconds - offset;

		resultDate = new Date(milliSeconds);
		System.out.println(sdf.format(resultDate));
	}
}
