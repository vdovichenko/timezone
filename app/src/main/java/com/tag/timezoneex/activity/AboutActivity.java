package com.tag.timezoneex.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import com.tag.timezoneex.R;

/**
 * Created by Mitya_Potemkin on 9/21/17.
 */

public class AboutActivity extends AppCompatActivity {

    private TextView tvPrivacy;
    private TextView tvTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_about);

        tvPrivacy = (TextView) findViewById(R.id.tvAboutPrivacy);
        tvPrivacy.setMovementMethod(LinkMovementMethod.getInstance());

        tvTerms = (TextView) findViewById(R.id.tvAboutTerms);
        tvTerms.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
