package com.tag.timezoneex.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.tag.timezoneex.R;
import com.tag.timezoneex.ads.AdsHelperFacade;
import com.tag.timezoneex.network.NetworkManager;

/**
 * Created by Mitya_Potemkin on 9/22/17.
 */

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.laout_splash);

        fetchAdCounters();

    }

    private void fetchAdCounters() {
        new NetworkManager(this).getAdsCounters(new NetworkManager.ICompleteListener() {
            @Override
            public void onComplete() {
                fetchAdInfo();
            }
        });
    }

    private void fetchAdInfo() {
        new NetworkManager(this).getAdsManagementInfo(new NetworkManager.ICompleteListener() {
            @Override
            public void onComplete() {
                //settings advertisement
                AdsHelperFacade.configure(getApplicationContext());
                AdsHelperFacade.createManagerAndPreload(getApplicationContext(), AdsHelperFacade.AdsType.INTERSTITIAL);
                AdsHelperFacade.createManagerAndPreload(getApplicationContext(), AdsHelperFacade.AdsType.REWARDED);

                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
