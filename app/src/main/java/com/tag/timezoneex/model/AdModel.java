package com.tag.timezoneex.model;

import com.tag.timezoneex.ads.IAdsHelper;

/**
 * Created by Mitya_Potemkin on 11/24/17.
 */

public class AdModel {
    public IAdsHelper getAdsHelper() {
        return adsHelper;
    }

    public void setAdsHelper(IAdsHelper adsHelper) {
        this.adsHelper = adsHelper;
    }

    public enum AdsOrder {
        HEIGHT(0), LOW(1);

        private final int value;
        AdsOrder(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private boolean isActive = true;
    private AdsOrder order = AdsOrder.HEIGHT;
    private IAdsHelper adsHelper;

    public AdModel() {
    }

    public AdModel(boolean isActive, AdsOrder order) {
        this.isActive = isActive;
        this.order = order;
    }

    public boolean isActive() {
        return isActive;
    }

    public AdsOrder getOrder() {
        return order;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public void setOrder(AdsOrder order) {
        this.order = order;
    }
}
