package com.tag.timezoneex.ads;

import android.content.Context;

import com.tag.timezoneex.SharedPreferencesHelper;
import com.tag.timezoneex.model.AdModel;

/**
 * Created by Mitya_Potemkin on 12/1/17.
 */

class AdsRewardedManager extends AdsManager implements IRewardedManager{

    @Override
    public void prepareAds(Context context) {
        super.prepareAds(context);
        SharedPreferencesHelper preferencesHelper = new SharedPreferencesHelper(context);
        maxCounter = preferencesHelper.getRewardedCounter() == 0 ? 0 : preferencesHelper.getRewardedCounter() - 1;

        for (AdModel adModel : adsHelpersList) {
            adModel.getAdsHelper().preloadRewarded(context);
        }
    }

    @Override
    public void showRewarded(Context context) {
        this.setListeners();
        if(this.actionsCounter == maxCounter) {
            if(adsHelpersList != null && (currentAdsHelperIndex < adsHelpersList.size())){
                adsHelpersList.get(currentAdsHelperIndex).getAdsHelper().getRewarded(context);
            }else {
                currentAdsHelperIndex = 0;
            }

        }else{
            this.actionsCounter++;
        }
    }

//    @Override
//    public void onError(String error) {
//        super.onError(error);
//        //try to show next ad from array
////        showRewarded(context);
//    }
}
