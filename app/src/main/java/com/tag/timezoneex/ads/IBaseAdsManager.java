package com.tag.timezoneex.ads;

import android.content.Context;

/**
 * Created by Mitya_Potemkin on 11/24/17.
 */

interface IBaseAdsManager {
    void prepareAds(Context context);
}
