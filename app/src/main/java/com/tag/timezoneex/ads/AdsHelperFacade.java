package com.tag.timezoneex.ads;

import android.content.Context;

/**
 * Created by Mitya_Potemkin on 12/1/17.
 */

public class AdsHelperFacade {

    public enum AdsType{
        INTERSTITIAL, REWARDED
    }
    private static IInterstitialManager interstitialManager;
    private static IRewardedManager rewardedManager;

    public static void configure(Context context) {
        AdsManager.configureNetworks(context);
    }

    synchronized public static void createManagerAndPreload(Context context, AdsType adsType) {
        switch (adsType){
            case INTERSTITIAL:
                if(interstitialManager == null){
                    interstitialManager = new AdsInterstitialManager();
                    interstitialManager.prepareAds(context);
                }
                break;

            case REWARDED:
                if(rewardedManager == null) {
                    rewardedManager = new AdsRewardedManager();
                    rewardedManager.prepareAds(context);
                }
                break;
        }
    }

    public static void showInterstitial(Context context) {
        if(interstitialManager != null) {
            interstitialManager.showInterstitial(context);
        }else{
            throw new RuntimeException("First have to createManagerAndPreload(Context context, AdsType adsType)");
        }
    }

    public static void showRewarded(Context context) {
        if(rewardedManager != null) {
            rewardedManager.showRewarded(context);
        }else{
            throw new RuntimeException("First have to createManagerAndPreload(Context context, AdsType adsType)");
        }
    }
}
