package com.tag.timezoneex.ads;

import android.content.Context;
import android.widget.Toast;

import com.tag.timezoneex.R;
import com.tag.timezoneex.SharedPreferencesHelper;
import com.tag.timezoneex.model.AdModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Mitya_Potemkin on 11/24/17.
 */

class AdsManager implements IBaseAdsManager, IAdsHelperListener{

//    private static AdsManager baseManager;


    private static final String TAG  = "AdsManager";
    //Show an interstitial ad unit every 3 time zone conversion
    //or
    //Show a rewarded ad unit every time the copy action limitation has reached (5 times)
    int actionsCounter = 0;

    static List<AdModel> adsHelpersList = new ArrayList<>();
    int currentAdsHelperIndex = 0;
    int maxCounter;
    private Context context;

    static void configureNetworks(Context context) {
        //adding ad's networks to the list
        SharedPreferencesHelper preferencesHelper = new SharedPreferencesHelper(context);

        //ToDo: will realize like as serializable array of ad's networks from SharedPreferences
        AdModel adModelApploving = preferencesHelper.getAdModelBy(SharedPreferencesHelper.AdsNetwork.APPLOVIN);
        AdModel adModelAppnext = preferencesHelper.getAdModelBy(SharedPreferencesHelper.AdsNetwork.APPNEXT);

        if(adModelApploving.isActive()){
            IAdsHelper applovingAdsHelper = new ApplovingAdsHelper();
            //initialize SDKs
            applovingAdsHelper.initSdk(context);

            adModelApploving.setAdsHelper(applovingAdsHelper);
            adsHelpersList.add(adModelApploving);
        }

        if(adModelAppnext.isActive()){
            IAdsHelper appnextAdsHelper = new AppnextAdsHelper();
            //initialize SDKs
            appnextAdsHelper.initSdk(context);

            adModelAppnext.setAdsHelper(appnextAdsHelper);
            adsHelpersList.add(adModelAppnext);

        }

        Collections.sort(adsHelpersList, new Comparator<AdModel>() {

            @Override
            public int compare(AdModel adModel1, AdModel adModel2) {
                return Integer.valueOf(adModel1.getOrder().getValue()).compareTo(adModel2.getOrder().getValue());
            }
        });
    }

    void setListeners() {
        for(AdModel adModel : adsHelpersList) {
            adModel.getAdsHelper().setAdsHelperListener(this);
        }
    }

    private void resetOrIncrementCurrentIndex() {
        if(currentAdsHelperIndex == adsHelpersList.size()-1) {
            this.currentAdsHelperIndex = 0;
        }else{
            this.currentAdsHelperIndex ++;
        }
    }

    /**
     * IBaseAdsManager
     **/
    @Override
    public void prepareAds(Context context) {
        this.context = context;
    }

    /**
     * IAdsHelperListener
    **/
    @Override
    public void onError(String error) {
        //moving to the next ad's network from array or reset index
        resetOrIncrementCurrentIndex();
        if(!"".equals(error)) {
            Toast.makeText(context, context.getString(R.string.no_ads) + "(" + error + ")", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context, context.getString(R.string.no_ads), Toast.LENGTH_SHORT).show();
        }
        this.actionsCounter = 0;
    }

    @Override
    public void onSuccess() {
        //reset interstitial or rewarded counter
        this.actionsCounter = 0;
        //moving to the next ad's network from array or reset index
        resetOrIncrementCurrentIndex();
    }
}
