package com.tag.timezoneex.ads;

import android.content.Context;

/**
 * Created by Mitya_Potemkin on 11/24/17.
 */

public interface IAdsHelper {
    void initSdk(Context context);
    void setAdsHelperListener(IAdsHelperListener helperListener);
    void preloadInterstitial(Context context);
    void preloadRewarded(Context context);
    void getInterstitial(Context context);
    void getRewarded(Context context);
}
