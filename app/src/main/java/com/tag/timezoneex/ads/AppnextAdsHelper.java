package com.tag.timezoneex.ads;

import android.content.Context;
import android.util.Log;

import com.appnext.ads.fullscreen.FullScreenVideo;
import com.appnext.ads.fullscreen.RewardedConfig;
import com.appnext.ads.fullscreen.RewardedVideo;
import com.appnext.ads.fullscreen.Video;
import com.appnext.ads.interstitial.Interstitial;
import com.appnext.ads.interstitial.InterstitialConfig;
import com.appnext.base.Appnext;
import com.appnext.core.callbacks.OnAdClicked;
import com.appnext.core.callbacks.OnAdClosed;
import com.appnext.core.callbacks.OnAdError;
import com.appnext.core.callbacks.OnAdLoaded;
import com.appnext.core.callbacks.OnAdOpened;
import com.appnext.core.callbacks.OnVideoEnded;

/**
 * Created by Mitya_Potemkin on 11/28/17.
 */

class AppnextAdsHelper extends AdsBaseHelper {
    private static final String TAG  = "AppnextAdsHelper";

    private final String APPNEXT_INTERSTITIAL_PLACEMENT_ID = "20452c4d-f7eb-4d2b-99cf-34f1447807b9";
    private final String APPNEXT_REWARDED_PLACEMENT_ID = "81c5cdbc-8d76-4666-a459-595ff9945a56";
    private Interstitial interstitial;
    private RewardedVideo rewarded;

    @Override
    public void initSdk(Context context) {
        Appnext.init(context);
    }

    @Override
    public void preloadInterstitial(Context context) {
        InterstitialConfig configInterstitial = new InterstitialConfig();
        configInterstitial.setAutoPlay(true);
        configInterstitial.setMute(true);
        configInterstitial.orientation = Interstitial.ORIENTATION_PORTRAIT;
        interstitial = new Interstitial(context, APPNEXT_INTERSTITIAL_PLACEMENT_ID, configInterstitial);
        interstitial.loadAd();

        // Get callback for ad loaded
        interstitial.setOnAdLoadedCallback(new OnAdLoaded() {
            @Override
            public void adLoaded(String s) {
                Log.d(TAG, "Interstitial adLoaded");
            }
        });
        // Get callback for ad opened
        interstitial.setOnAdOpenedCallback(new OnAdOpened() {
            @Override
            public void adOpened() {
                Log.d(TAG, "Interstitial adOpened");
                AppnextAdsHelper.this.helperListener.onSuccess();
            }
        });
        // Get callback for ad clicked
        interstitial.setOnAdClickedCallback(new OnAdClicked() {
            @Override
            public void adClicked() {
                Log.d(TAG, "Interstitial adClicked");
            }
        });
        // Get callback for ad closed
        interstitial.setOnAdClosedCallback(new OnAdClosed() {
            @Override
            public void onAdClosed() {
                Log.d(TAG, "Interstitial onAdClosed");
            }
        });
        // Get callback for ad error
        interstitial.setOnAdErrorCallback(new OnAdError() {
            @Override
            public void adError(String error) {
                Log.d(TAG, "Interstitial adError("+error+")");
                interstitialErrorCode = error;
            }
        });
    }

    @Override
    public void preloadRewarded(final Context context) {
        RewardedConfig configRewarded = new RewardedConfig();
        configRewarded.setMode(RewardedVideo.VIDEO_MODE_MULTI);
        configRewarded.setMultiTimerLength(8);
        configRewarded.setVideoLength(Video.VIDEO_LENGTH_SHORT);
        configRewarded.setRollCaptionTime(-1);
        configRewarded.setOrientation(FullScreenVideo.ORIENTATION_LANDSCAPE);
        rewarded = new RewardedVideo(context, APPNEXT_REWARDED_PLACEMENT_ID, configRewarded);
        rewarded.loadAd();

        // Get callback for ad loaded
        rewarded.setOnAdLoadedCallback(new OnAdLoaded() {
            @Override
            public void adLoaded(String bannerID) {
                Log.d(TAG, "Rewarded adLoaded");
            }
        });

        // Get callback for ad opened
        rewarded.setOnAdOpenedCallback(new OnAdOpened() {
            @Override
            public void adOpened() {
                Log.d(TAG, "Rewarded adOpened");
            }
        });
        // Get callback for ad clicked
        rewarded.setOnAdClickedCallback(new OnAdClicked() {
            @Override
            public void adClicked() {
                Log.d(TAG, "Rewarded adClicked");
            }
        });

        // Get callback for ad closed
        rewarded.setOnAdClosedCallback(new OnAdClosed() {
            @Override
            public void onAdClosed() {
                Log.d(TAG, "Rewarded onAdClosed");
                preloadRewarded(context);
            }
        });

        // Get callback for ad error
        rewarded.setOnAdErrorCallback(new OnAdError() {
            @Override
            public void adError(String error) {
                Log.d(TAG, "Rewarded adError("+error+")");
                rewardedErrorCode = error;
            }
        });

        // Get callback when the user saw the video until the end (video ended)
        rewarded.setOnVideoEndedCallback(new OnVideoEnded() {
            @Override
            public void videoEnded() {
                Log.d(TAG, "Rewarded videoEnded");
            }
        });
    }

    @Override
    public void getInterstitial(Context context) {
        if(interstitial.isAdLoaded()) {
            interstitial.showAd();
//            this.helperListener.onSuccess();

        }else{
            // No ad is available to display.  Perform failover logic...
            Log.d(TAG, "Interstitial not available");
            this.helperListener.onError(interstitialErrorCode);
        }
    }

    @Override
    public void getRewarded(Context context) {
        if (rewarded.isAdLoaded()){
            rewarded.showAd();
            AppnextAdsHelper.this.helperListener.onSuccess();

        }else{
            // No ad is available to display.  Perform failover logic...
            Log.d(TAG, "Rewarded not available");
            this.helperListener.onError(rewardedErrorCode);
        }
    }

    @Override
    public void setAdsHelperListener(IAdsHelperListener helperListener) {
        this.helperListener = helperListener;
    }
}
