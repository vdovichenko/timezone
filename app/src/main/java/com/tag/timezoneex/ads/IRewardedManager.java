package com.tag.timezoneex.ads;

import android.content.Context;

/**
 * Created by Mitya_Potemkin on 12/2/17.
 */

interface IRewardedManager extends IBaseAdsManager{
    void showRewarded(Context context);
}
