package com.tag.timezoneex.ads;

import android.content.Context;

import com.tag.timezoneex.SharedPreferencesHelper;
import com.tag.timezoneex.model.AdModel;

/**
 * Created by Mitya_Potemkin on 12/1/17.
 */

class AdsInterstitialManager extends AdsManager implements IInterstitialManager {

    @Override
    public void prepareAds(Context context) {
        super.prepareAds(context);
        SharedPreferencesHelper preferencesHelper = new SharedPreferencesHelper(context);
        maxCounter = preferencesHelper.getInterstitialCounter() == 0 ? 0 : preferencesHelper.getInterstitialCounter() - 1;

        for (AdModel adModel : adsHelpersList) {
            adModel.getAdsHelper().preloadInterstitial(context);
        }
    }

    @Override
    public void showInterstitial(Context context) {
        this.setListeners();
        if(this.actionsCounter == maxCounter) {
            if(adsHelpersList != null && (currentAdsHelperIndex < adsHelpersList.size())){
                adsHelpersList.get(currentAdsHelperIndex).getAdsHelper().getInterstitial(context);
            }else{
                currentAdsHelperIndex = 0;
                this.actionsCounter = 0;
            }

        }else{
            this.actionsCounter++;
        }
    }

//    @Override
//    public void onError(String error) {
//        super.onError(error);
//        //try to show next ad from array
////        showInterstitial(context);
//    }
}
