package com.tag.timezoneex.ads;

import android.content.Context;
import android.util.Log;

import com.applovin.adview.AppLovinIncentivizedInterstitial;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinSdk;

/**
 * Created by Mitya_Potemkin on 10/25/17.
 */

class ApplovingAdsHelper extends AdsBaseHelper {
    private static final String TAG  = "ApplovingAdsHelper";

    private AppLovinIncentivizedInterstitial rewardedAd;
    private AppLovinInterstitialAdDialog interstitialAd;

    @Override
    public void initSdk(Context context) {
        AppLovinSdk.initializeSdk(context);
    }

    @Override
    public void preloadRewarded(Context context) {
        rewardedAd = AppLovinIncentivizedInterstitial.create(context);
        rewardedAd.preload(new AppLovinAdLoadListener() {
            @Override
            public void adReceived(AppLovinAd appLovinAd) {
                Log.d(TAG, "rewarded adReceived");
            }

            @Override
            public void failedToReceiveAd(int i) {
                Log.d(TAG, "rewarded failedToReceiveAd");
                rewardedErrorCode = Integer.toString(i);
            }
        });
    }

    @Override
    public void preloadInterstitial(Context context) {
        //loaded when initializing SDK
        interstitialAd = AppLovinInterstitialAd.create(AppLovinSdk.getInstance(context), context);
        interstitialAd.setAdLoadListener(new AppLovinAdLoadListener() {
            @Override
            public void adReceived(AppLovinAd appLovinAd) {
                Log.d(TAG, "interstitial adReceived");
            }

            @Override
            public void failedToReceiveAd(int i) {
                Log.d(TAG, "interstitial failedToReceiveAd");
                interstitialErrorCode = Integer.toString(i);
            }
        });
    }

    @Override
    public void getInterstitial(Context context) {
        if (interstitialAd.isAdReadyToDisplay()) {
            // An ad is available to display.  It's safe to call show.
            interstitialAd.show();
            Log.d(TAG, "Interstitial showed");
            this.helperListener.onSuccess();

        } else {
            // No ad is available to display.  Perform failover logic...
            Log.d(TAG, "Interstitial not available");
            this.helperListener.onError(interstitialErrorCode);
        }
    }

    @Override
    public void getRewarded(final Context context) {
        if (this.rewardedAd.isAdReadyToDisplay()) {
            // An ad is available to display.  It's safe to call show.

            this.rewardedAd.show(context, null, null, new AppLovinAdDisplayListener() {
                @Override
                public void adDisplayed(AppLovinAd appLovinAd) {
                    // A rewarded video is being displayed.
                    Log.d(TAG, "Rewarded is being displayed");
                }
                @Override
                public void adHidden(AppLovinAd appLovinAd) {
                    // A rewarded video was closed.  Preload the next video now.  We won't use a load listener.
                    preloadRewarded(context);
                }
            });
            this.helperListener.onSuccess();

        } else {
            // No ad is available to display.  Perform failover logic...
            Log.d(TAG, "Rewarded not available");
            this.helperListener.onError(rewardedErrorCode);
        }
    }

    @Override
    public void setAdsHelperListener(IAdsHelperListener helperListener) {
        this.helperListener = helperListener;
    }
}
