package com.tag.timezoneex.ads;

/**
 * Created by Mitya_Potemkin on 11/24/17.
 */

abstract class AdsBaseHelper implements IAdsHelper {
    protected static final String TAG  = "AdsBaseHelper";

    IAdsHelperListener helperListener;

    String rewardedErrorCode = "";
    String interstitialErrorCode = "";
}
