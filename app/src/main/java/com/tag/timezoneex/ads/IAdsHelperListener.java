package com.tag.timezoneex.ads;

/**
 * Created by Mitya_Potemkin on 12/1/17.
 */

interface IAdsHelperListener {
    void onError(String error);
    void onSuccess();
}
