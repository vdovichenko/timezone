package com.tag.timezoneex;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.text.ClipboardManager;

/**
 * Created by Mitya_Potemkin on 9/21/17.
 */

public class Utils {

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static Boolean copyToClipboard (Context context, String text) {

        try {
            int sdk = Build.VERSION.SDK_INT;
            if (sdk < Build.VERSION_CODES.HONEYCOMB) {
                ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboardManager.setText(text);

            } else {
                android.content.ClipboardManager clipboardManager = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clipData = android.content.ClipData.newPlainText(context.getString(R.string.app_name), text);
                clipboardManager.setPrimaryClip(clipData);
            }

            return true;

        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
