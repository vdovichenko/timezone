package com.tag.timezoneex;

import android.content.Context;
import android.content.SharedPreferences;

import com.tag.timezoneex.model.AdModel;

/**
 * Created by Mitya_Potemkin on 11/24/17.
 */

public class SharedPreferencesHelper {

    public enum AdsNetwork {
        APPLOVIN, APPNEXT
    }

    private final String KEY_INTERSTITIAL_COUNTER = "KEY_INTERSTITIAL_COUNTER";
    private final String KEY_REWARDED_COUNTER = "KEY_REWARDED_COUNTER";
    private final String KEY_APPLOVING_ACTIVE = "KEY_APPLOVING_ACTIVE";
    private final String KEY_APPLOVING_ORDER = "KEY_APPLOVING_ORDER";
    private final String KEY_APPNEXT_ACTIVE = "KEY_APPNEXT_ACTIVE";
    private final String KEY_APPNEXT_ORDER = "KEY_APPNEXT_ORDER";

    public final int DEFAULT_INTERSTITIAL_COUNTER = 2;
    public final int DEFAULT_REWARDED_COUNTER = 2;
    private final boolean DEFAULT_AD_NETWORK_ACTIVE = true;
    private final int DEFAULT_APPLOVING_ORDER = 0;
    private final int DEFAULT_APPNEXT_ORDER = 1;

    private final SharedPreferences mPreferences;

    public SharedPreferencesHelper(Context context) {
        mPreferences = context.getSharedPreferences(context.getPackageName(), 0);
    }

    public void saveAdsCounters(int interstitial, int rewarded){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putInt(KEY_INTERSTITIAL_COUNTER, interstitial);
        editor.putInt(KEY_REWARDED_COUNTER, rewarded);
        editor.apply();
    }

    public void saveAdsManagmentInfo(AdsNetwork adsNetwork, AdModel adModel){
        SharedPreferences.Editor editor = mPreferences.edit();
        switch (adsNetwork) {
            case APPLOVIN:
                editor.putBoolean(KEY_APPLOVING_ACTIVE, adModel.isActive());
                editor.putInt(KEY_APPLOVING_ORDER, adModel.getOrder().getValue());
                break;

            case APPNEXT:
                editor.putBoolean(KEY_APPNEXT_ACTIVE, adModel.isActive());
                editor.putInt(KEY_APPNEXT_ORDER, adModel.getOrder().getValue());
                break;
        }

        editor.apply();
    }

    public int getInterstitialCounter() {
        return mPreferences.getInt(KEY_INTERSTITIAL_COUNTER, DEFAULT_INTERSTITIAL_COUNTER);
    }

    public int getRewardedCounter() {
        return mPreferences.getInt(KEY_INTERSTITIAL_COUNTER, DEFAULT_REWARDED_COUNTER);
    }

    public AdModel getAdModelBy(AdsNetwork adsNetwork) {
        AdModel adModel = new AdModel();
        switch (adsNetwork) {
            case APPLOVIN:
                adModel.setActive(mPreferences.getBoolean(KEY_APPLOVING_ACTIVE, DEFAULT_AD_NETWORK_ACTIVE));
                if(mPreferences.getInt(KEY_APPLOVING_ORDER, DEFAULT_APPLOVING_ORDER) == 0) {
                    adModel.setOrder(AdModel.AdsOrder.HEIGHT);
                }else{
                    adModel.setOrder(AdModel.AdsOrder.LOW);
                }
                break;

            case APPNEXT:
                adModel.setActive(mPreferences.getBoolean(KEY_APPNEXT_ACTIVE, DEFAULT_AD_NETWORK_ACTIVE));
                if(mPreferences.getInt(KEY_APPNEXT_ORDER, DEFAULT_APPNEXT_ORDER) == 0) {
                    adModel.setOrder(AdModel.AdsOrder.HEIGHT);
                }else{
                    adModel.setOrder(AdModel.AdsOrder.LOW);
                }
                break;
        }

        return adModel;
    }
}
