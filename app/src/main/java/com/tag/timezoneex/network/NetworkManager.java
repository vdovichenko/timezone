package com.tag.timezoneex.network;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.tag.timezoneex.SharedPreferencesHelper;
import com.tag.timezoneex.model.AdModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Mitya_Potemkin on 11/24/17.
 */

public class NetworkManager {
    private static final String TAG = "NetworkManager";

    public interface ICompleteListener {
        void onComplete();
    }

    private final String baseUrl = "https://cdn.mobilemag.me/simple/config";
    private final String getAdsCounters = "/ads.json";
    private final String getAdsManagmentInfo = "/control.json";

    private final RequestQueue mQueue;
    private final SharedPreferencesHelper mSharedPreferencesHelper;
    private Context mContext;

    public NetworkManager(Context context) {
        this.mQueue = Volley.newRequestQueue(context);
        this.mSharedPreferencesHelper = new SharedPreferencesHelper(context);
        this.mContext = context;
    }

    public void getAdsCounters(final ICompleteListener completeListener) {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, baseUrl + getAdsCounters, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    //TEST
//                    JSONObject object = new JSONObject("{\"placements\":{\"interstitial\":0,\"rewarded\":1}}");
//                    JSONObject placements = object.getJSONObject("placements");
                    //TEST end
                    JSONObject placements = response.getJSONObject("placements");
                    mSharedPreferencesHelper.saveAdsCounters(
                            placements.optInt("interstitial", mSharedPreferencesHelper.DEFAULT_INTERSTITIAL_COUNTER),
                            placements.optInt("rewarded", mSharedPreferencesHelper.DEFAULT_REWARDED_COUNTER));

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "Get ads counters error", Toast.LENGTH_SHORT).show();
                }

                completeListener.onComplete();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext, "Get ads counters error", Toast.LENGTH_SHORT).show();
                completeListener.onComplete();
            }
        });
        mQueue.add(jsonObjectRequest);
    }

    public void getAdsManagementInfo(final ICompleteListener completeListener) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, baseUrl + getAdsManagmentInfo, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    //TEST
//                    JSONObject object = new JSONObject("{ \"players\": [ {\"type\": \"A\", \"active\": false, \"order\": 0}, {\"type\": \"B\", \"active\": true, \"order\": 1}]}");
//                    JSONArray players = object.getJSONArray("players");
                    //TEST end
                    JSONArray players = response.getJSONArray("players");
                    for(int i=0; i<players.length(); i++) {
                        JSONObject player = players.getJSONObject(i);
                        AdModel adModel = new AdModel(player.getBoolean("active"), player.getInt("order") == 0 ? AdModel.AdsOrder.HEIGHT : AdModel.AdsOrder.LOW);
                        SharedPreferencesHelper.AdsNetwork network = SharedPreferencesHelper.AdsNetwork.APPLOVIN;//By default AppLovin
                        switch (player.getString("type")) {
                            case "A":
                                network = SharedPreferencesHelper.AdsNetwork.APPLOVIN;
                                break;

                            case "B":
                                network = SharedPreferencesHelper.AdsNetwork.APPNEXT;
                                break;
                        }
                        mSharedPreferencesHelper.saveAdsManagmentInfo(network, adModel);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "Get ads info error", Toast.LENGTH_SHORT).show();
                }
                completeListener.onComplete();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext, "Get ads info error", Toast.LENGTH_SHORT).show();
                completeListener.onComplete();
            }
        });
        mQueue.add(jsonObjectRequest);
    }
}
